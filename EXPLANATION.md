## List harga

### Web
Tampilan list harga menggunakan table dan setiap kolom ada kolom pencarian, kemudian pada kolom paling kanan terdapat 2 tombol aksi, yaitu edit dan hapus, dan dibawah table terdapat informasi halam ini menampilkan berapa data dari total berapa data dan pilihan untuk mengganti halaman data.
bentuk tampilan seperti ini akan memudahkan user untuk melihat beberapa data sekaligus dan kolom pencarian pada setiap kolom akan memudahkan user untuk mencari data dengan lebih spesifik. 2 tombol aksi ditempatkan di kolom paling kanan dari tabel akan memudahkan user untuk melakukan aksi yang diinginkan permasing masing data. total data akan membuat user mempunyai gambaran berapa banyak data yang sudah ada, terutama ini akan sangat membantu ketika melakukan pencarian spesifik.

### Mobile
Tampilan list harga pada mobile akan berubah menjadi card ini digunakan agar data data yang harus ditampilkan dapat dilihat secara langsung oleh user, dan dibagian bawah masing masing card terdapat tombol edit dan hapus, peletakan tombol hapus berada disebelah kiri karena mengurangi resiko terjadinya ketidak sengajaan pemencetan tombol hapus, 
Sedangkan informasi total data dan aksi penggantian halaman data diletakkan atas dan dibawah list data, ini untuk memudahkan user ketika ingin melakukan penggantian data ketika user sedang melakukan scrolling


## Input Data
Masuk kedalam halaman lain setelah user meng-klik tombol Buat Harga
Isian dropdown dibuat bisa search bertujuan untuk membantu user memilih isian yang akan diambil, karena pilihan isian dapat banyak sekali.
Semua form wajib diisi dan ketika klik tombol Simpan tetapi ada kolom yang belum diisi akan ada peringatan dibawah kolom yang belum diisi bahwa kolom tersebut harus diisi, peringatan ini dibuat untuk membantu user dalam mengetahui secara spesifik kolom mana yang harus user isi.


## Edit Data
Masuk kedalam halaman lain setelah user meng-klik tombol Edit pada masing masing row data
Secara default akan berisi data yang sekarang sudah ada, dan user tinggal mengganti pada kolom yang diinginkan, hal ini bertujuan agar user tidak mengisi data data dari awal sedangkan dia hanya ingin mengganti sebagian data


## Hapus Data
terdapat PopUp konfirmasi hapus data setelah user meng-klik tombol hapus, ini berfungsi agar user tidak melakukan penghapusan data secara tidak sengaja dan untuk lebih yakin dalam menghapus data


## Notifikasi Konfirmasi aksi

### Aksi Berhasil
Setelah user melakukan konfirmasi aksi dan aksi tersebut berhasil maka akan diarahkan kembali ke halaman list harga dan kemudian di paling atas halaman akan ada banner tanda berhasil. dikembalikan ke halaman list harga dikarenakan semua perubahan akan berpengaruh terhadap list harga

### Aksi Gagal
Banner pemberitahuan aksi gagal terdapat pada halaman masing masing tempat aksi, hal ini bertujuan agar ketika user ada kesalahan dapat langsung memperbaiki
