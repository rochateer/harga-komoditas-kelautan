# Harko (Harga Komoditas)
Website untuk berbagi harga komoditas

## Demo
Akses website melalui [Harko](https://epic-liskov-60555c.netlify.app/)

### Input Data Harga
1.  Klik tombol "_Buat Harga_".
2.  Setelah masuk pada halaman "Tambah Harga" masukkan informasi informasi terkat, semua kolom **Harus** terisi.
3.  Klik tombol "_Simpan_" untuk menyimpan informasi harga komoditas.

### Merubah Data Harga
1.  Klik tombol "_Edit_" pada tabel list harga pada baris data harga komoditas yang akan diedit.
2.  Setelah masuk pada halaman edit, edit pada kolom yang akan di edit.
3.  Klik tombol "_Simpan Perubahan_" untuk menyimpan perubahan harga komoditas.

### Menghapus Data Harga
1.  Klik tombol "_Hapus_" pada tabel list harga pada baris data harga komoditas yang akan dihapus.
2.  Kemudian akan muncul popup konfirmasi penghapusan data.
3.  Klik tombol "_Hapus_" pada popup konfirmasi untuk menghapus harga komoditas.



## Development
The website using ReactJS

1. Clone the repository and move into:
```bash
git https://gitlab.com/rochateer/task-fe-efishery.git
cd task-fe-efishery
```
or you can change the folder name by your self

2. Install:
```bash
npm Install
```

3. Start the project
```bash
npm start
```

4. If you wanna build for production
```bash
npm run build
```

### Development (Code)
#### View
if you wanna change or adding view go to the **\src\views**

#### Default Page
All of default page like notification banner or etc inside folder **\src\views\pageDefault**

#### Helper
Helper or Default function is place you will put all of function will be able in everywhere, so yo dont need to rewrite same function for same purpose, the folder is **\src\Helper**

#### Styling
for adding some additional style or change current style, the folder is **\src\scss\** adn the main file for styling is __style.scss__
