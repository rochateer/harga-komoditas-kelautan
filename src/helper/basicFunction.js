import Fuse from 'fuse.js';

export function fuzzySearch(options) {
    const fuse = new Fuse(options, {
        keys: ['name', 'value'],
        threshold: 0.3,
    });

    return (value) => {
        if (!value.length) {
            return options;
        }
        const searchFuse = fuse.search(value);
        if(searchFuse.length > 0){
           const returnAda = searchFuse.map(sf => sf.item);
           return returnAda;
        }else{
          return []
        }
    };
}

export function changeToFormatNumber(props) {
  //add delimiter to number
  if (props !== undefined && props !== null) {
    // return formatNumber(props, '.', ',');
    return parseFloat(props).toLocaleString();
  } else {
    return null;
  }
}

export function changeValuetoString(props) {
  //Swap undefined or null to empty string
  if (props === undefined || props === null) {
    return '';
  } else {
    return props.toString();
  }
}

export const convertDateFormattoString = (jsondate) => {
  if(jsondate === undefined || jsondate === null){
    return null
  }else{
    let date = new Date(jsondate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    dt = dt.toString().padStart(2, "0");
    month = month.toString().padStart(2, "0");

    return year + "-" + month + "-" + dt;
  }
};

export const generateDummyUUIDTask = () => {
  return 'xxxxxxxx-TASK-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[x]/g, (c) => {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}