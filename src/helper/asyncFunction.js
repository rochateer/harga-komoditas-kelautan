import axios from "axios";

const API_URL = "https://stein.efishery.com/v1/storages/5e1edf521073e315924ceab4";

export const getDatafromAPI = async (url) => {
  try {
    let respond = await axios.get(API_URL + url);
    if (respond.status >= 200 && respond.status < 300) {
    //   console.log("respond GET Data", respond);
    }
    return respond;
  } catch (err) {
    let respond = err;
    // console.log("respond GET Data err", err);
    return respond;
  }
};

export const postDatatoAPI = async (url, data) => {
  try {
    let respond = await axios.post(API_URL + url, data, {
      headers: {
        "Content-Type": "application/json"
      },
    });
    if (respond.status >= 200 && respond.status < 300) {
    //   console.log("respond Post Data", respond);
    }
    return respond;
  } catch (err) {
    let respond = err;
    // console.log("respond Post Data err", err);
    return respond;
  }
};

export const putDatatoAPI = async (url, data) => {
  try {
    let respond = await axios.put(API_URL + url, data, {
      headers: {
        "Content-Type": "application/json"
      },
    });
    if (respond.status >= 200 && respond.status < 300) {
    //   console.log("respond patch Data", respond);
    }
    return respond;
  } catch (err) {
    let respond = err;
    // console.log("respond patch Data err", err);
    return respond;
  }
};

export const deleteDatafromAPI = async (url, data) => {
  try {
    let respond = await axios.delete(API_URL + url, {
      headers: {
        "Content-Type": "application/json"
      },
      data: data
    });
    if (respond.status >= 200 && respond.status < 300) {
    //   console.log("respond delete Data", respond);
    }
    return respond;
  } catch (err) {
    let respond = err;
    // console.log("respond delete Data err", err);
    return respond;
  }
};