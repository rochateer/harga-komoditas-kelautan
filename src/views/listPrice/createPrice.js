import React, {Component} from 'react';
import { getDatafromAPI, postDatatoAPI } from "../../helper/asyncFunction";
import { fuzzySearch, generateDummyUUIDTask } from "../../helper/basicFunction";
import SelectSearch from 'react-select-search';
import { Redirect } from 'react-router-dom';

const filter_key = ["komoditas", "area_provinsi", "area_kota", "size", "price"];

const DefaultNotif = React.lazy(() =>
  import("../pageDefault/defaultNotif")
);

class CreatePrice extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect_sign : false,
      action_message : null,
      action_status : null,
      option_area : [],
      option_size : [],
      province_selection : [],
      city_selection : [],
      size_selection : [],
      form_data : {},
      handle_checking_form : {},
    };
  }

  componentDidMount(){
    this.getAreaList();
    this.getSizeList();
  }

  getAreaList(){
    getDatafromAPI('/option_area').then(res => {
      if (res !== undefined && res.data !== undefined) {
        const items = res.data;
        let selection_province = [];
        [...new Set(items.map(({ province }) => province))].map(it => selection_province.push({value : it, name : it}));
        this.setState({option_area : items, province_selection : selection_province});
      }
    })
  }

  getSizeList(){
    getDatafromAPI('/option_size').then(res => {
      if (res !== undefined && res.data !== undefined) {
        const items = res.data;
        let selection_size = [];
        [...new Set(items.map(({ size }) => size))].map(it => selection_size.push({value : it, name : it}));
        this.setState({option_size : items, size_selection : selection_size});
      }
    })
  }

  handleChangeForm = (e) => {
    let dataForm = {...this.state.form_data};
    const name = e.target.name;
    const value = e.target.value;
    dataForm[name] = value;
    this.setState({form_data : dataForm});
  }

  onChangeProvince = (e) => {
    const value = e;
    if(value !== undefined && value !== null){
      let dataForm = {...this.state.form_data};
      const dataArea = this.state.option_area.filter(oa => oa.province === value);
      let selection_city = [];
      [...new Set(dataArea.map(({ city }) => city))].map(it => selection_city.push({value : it, name : it}));
      dataForm["area_provinsi"] = value;
      this.setState({city_selection : selection_city, form_data : dataForm});
    }
  }

  onChangeCity = (e) => {
    const value = e;
    if(value !== undefined && value !== null){
      let dataForm = {...this.state.form_data};
      dataForm["area_kota"] = value;
      this.setState({form_data : dataForm});
    }
  }

  onChangeSize = (e) => {
    const value = e;
    if(value !== undefined && value !== null){
      let dataForm = {...this.state.form_data};
      dataForm["size"] = value;
      this.setState({form_data : dataForm});
    }
  }

  handleCheckingForm(){
    let countErr = 0;
    let dataForm = this.state.form_data;
    let handleCheckingForm = {};
    for(let i = 0; i < filter_key.length; i++){
      if(dataForm[filter_key[i]] === undefined || dataForm[filter_key[i]] === null || dataForm[filter_key[i]].length === 0){
        countErr++;
        handleCheckingForm[filter_key[i]] = true;
      }
    }
    this.setState({ handle_checking_form : handleCheckingForm});
    return countErr;
  }

  saveDataHarga = async() => {
    this.setState({action_status : null, action_message : null})
    const checkingForm = this.handleCheckingForm();
    if(checkingForm === 0){
      let dataForm = {...this.state.form_data};
      dataForm["tgl_parsed"] = new Date();
      dataForm["timestamp"] = new Date();
      dataForm["uuid"] = generateDummyUUIDTask();
      const res = await postDatatoAPI('/list', [dataForm]);
      if(res !== undefined && res.data !== undefined){
        this.setState({redirect_sign : true, action_status : 'success', action_message : 'Data anda berhasil disimpan'})
      }else{
        this.setState({action_status : 'failed', action_message : 'Terjadi kesalahan, silahkan coba lagi'})
      }
    }
  }

  render() {
    if(this.state.redirect_sign !== false){
      return (
        <Redirect to={{
          pathname: "/",
          state: { action_status: this.state.action_status, action_message: this.state.action_message }
          }}
        />
      );
    }
    return(
      <div>
        <DefaultNotif
          actionMessage={this.state.action_message}
          actionStatus={this.state.action_status}
        />
        <div className="row row--mr-0">
          <div className="col">
            <div className="card">
              <div className="card-header">
                Buat Harga
              </div>
              <div className="card-body">
                <form className="row g-3 form-create-price form-create-price--row-mb-20">
                  <div className="row">
                    <div className="col-xs-12 col-md-6 col-lg-4">
                      <label htmlFor="komoditas" className="form-label">Komoditas</label>
                      <input type="text" className="form-control" id="komoditas" name={'komoditas'} onChange={this.handleChangeForm} value={this.state.form_data.komoditas} />
                      {this.state.handle_checking_form.komoditas === true && (<span className="form-create-price__err-not-fill">Please fill this form</span>)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-12 col-md-6 col-lg-4">
                      <label htmlFor="provinsi" className="form-label">Provinsi</label>
                      <SelectSearch 
                        options={this.state.province_selection} 
                        search  
                        onChange={this.onChangeProvince}
                        filterOptions={fuzzySearch}
                        id="area_provinsi"
                      />
                      {this.state.handle_checking_form.area_provinsi === true && (<span className="form-create-price__err-not-fill">Please fill this form</span>)}
                    </div>
                    <div className="col-xs-12 col-md-6 col-lg-4">
                      <label htmlFor="kota" className="form-label">Kota</label>
                      <SelectSearch 
                        options={this.state.city_selection} 
                        search  
                        onChange={this.onChangeCity}
                        filterOptions={fuzzySearch}
                        placeholder={this.state.form_data.area_provinsi === undefined ? "Pilih provinsi terlebih dahulu" : ""}
                        id="area_kota"
                      />
                      {this.state.handle_checking_form.area_kota === true && (<span className="form-create-price__err-not-fill">Please fill this form</span>)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-12 col-md-6 col-lg-4">
                      <label htmlFor="size" className="form-label">Size</label>
                      <SelectSearch 
                        options={this.state.size_selection} 
                        search  
                        onChange={this.onChangeSize}
                        filterOptions={fuzzySearch}
                        id="size"
                      />
                      {this.state.handle_checking_form.size === true && (<span className="form-create-price__err-not-fill">Please fill this form</span>)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-12 col-md-6 col-lg-4">
                      <label htmlFor="harga" className="form-label">Harga</label>
                      <input type="number" className="form-control" id="price" name={'price'} onChange={this.handleChangeForm} value={this.state.form_data.price} />
                      {this.state.handle_checking_form.price === true && (<span className="form-create-price__err-not-fill">Please fill this form</span>)}
                    </div>
                  </div>
                </form>
                <div className="row row--mb-0">
                  <div className="col-xs-12 col-md-4 form-create-price__submit--center">
                    <button onClick={this.saveDataHarga} className="btn btn-success">Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

  export default CreatePrice;
  