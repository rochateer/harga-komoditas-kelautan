import React, {Component} from 'react';
import { getDatafromAPI, deleteDatafromAPI } from "../../helper/asyncFunction";
import { changeToFormatNumber, changeValuetoString, convertDateFormattoString } from "../../helper/basicFunction";
import Pagination from 'react-js-pagination';
import { Link } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'

const filter_key = ["komoditas", "area_provinsi", "area_kota", "size", "price"];

const DefaultNotif = React.lazy(() =>
  import("../pageDefault/defaultNotif")
);

class ListPrice extends Component {

  constructor(props) {
    super(props);
    this.state = {
      action_message : null,
      action_status : null,
      activePage: 1,
      totalData: 0,
      perPage: 10,
      price_list : [],
      price_list_pagination : [],
      filter_list : {},
      id_delete : null,
      modal_notify_delete : false
    };
  }

  componentDidMount(){
    this.getPriceList();
    
    if(this.props.location.state !== undefined){
      if(this.props.location.state.action_status !== undefined && this.props.location.state.action_status !== null){
        const history = createHistory();
        this.setState({action_status : this.props.location.state.action_status, action_message : this.props.location.state.action_message}, () =>{
          history.replace({...history.location.state, undefined});
        })
      }
    }
  }

  getPriceList(){
    getDatafromAPI('/list').then(res => {
      if (res !== undefined && res.data !== undefined) {
        const items = res.data.filter(rd => rd.uuid !== null).sort(function(a,b){
          return new Date(b.tgl_parsed) - new Date(a.tgl_parsed);
        });
        this.setState({price_list : items, totalData : items.length}, () => {
          this.viewPagination();
        });
      }
    })
  }

  handleFilterList = (e) => {
    const index = e.target.name;
    let value = e.target.value;
    if (value !== null && value.length === 0) {
      value = null;
    }
    let dataFilter = this.state.filter_list;
    dataFilter[index] = value;
    this.setState({ filter_list: dataFilter, activePage: 1 }, () => {
      this.filterDataManual();
    })
  }

  filterDataManual = () => {
    const dataFilter = this.state.filter_list;
    let list_price_filter = JSON.parse(JSON.stringify(this.state.price_list));
    for(let i = 0; i < filter_key.length; i++){
      if(dataFilter[filter_key[i]] !== undefined && dataFilter[filter_key[i]] !== null){
        const regex = new RegExp(dataFilter[filter_key[i]], 'gi');
        list_price_filter = list_price_filter.filter(bf => changeValuetoString(bf[filter_key[i]]).match(regex));
      }
    }
    this.setState({totalData : list_price_filter.length})
    this.viewPagination(list_price_filter);
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber }, () => {
      this.viewPagination();
    });
  }

  viewPagination(price_list_filter){
    let list_price_all = [];
    if(price_list_filter === undefined){
      list_price_all = JSON.parse(JSON.stringify(this.state.price_list));
    }else{
      list_price_all = JSON.parse(JSON.stringify(price_list_filter));
    }
    let perPage = this.state.perPage;
    let list_price_page = [];
    if(perPage <= list_price_all.length){
      let pageNow = this.state.activePage-1;
      list_price_page = list_price_all.slice(pageNow * perPage, (pageNow+1)*perPage);
    }else{
      list_price_page = list_price_all;
    }
    this.setState({price_list_pagination : list_price_page})
  }

  loopSearchBar = () => {
    let searchBar = [];
    for (let i = 0; i < filter_key.length; i++) {
      searchBar.push(
        <td className="search-col">
          <div className="input-group input-group-sm mb-3">
            <input type="text" className="form-control" onChange={this.handleFilterList} value={this.state.filter_list[filter_key[i]]} name={filter_key[i]} aria-label={filter_key[i]} aria-describedby="basic-addon1" />
          </div>
        </td>
      )
    }
    return searchBar;
  }

  toggleNotifyDeleteData = (id) => {
    if(id !== undefined){
      this.setState({id_delete : id});
    }else{
      this.setState({id_delete : null});
    }
    this.setState((prevState) => ({
      modal_notify_delete: !prevState.modal_notify_delete,
    }));
  }
  
  deleteDataHarga = async() => {
    this.toggleNotifyDeleteData();
    this.setState({action_status : null, action_message : null});
    if(this.state.id_delete !== undefined && this.state.id_delete !== null){
      const dataApiDel = {"condition": {"uuid": this.state.id_delete}};
      const res = await deleteDatafromAPI('/list', dataApiDel);
      if(res !== undefined && res.data !== undefined){
        this.setState({action_status : 'success', action_message : 'Data anda berhasil disimpan, silahkan reload halaman anda'})
      }else{
        this.setState({action_status : 'failed', action_message : 'Terjadi kesalahan, silahkan coba lagi'})
      }
    }else{
      this.setState({action_status : 'failed', action_message : 'Pilih data terlebih dahulu'})
    }
  }

  render() {
    return(
      <div>
        <DefaultNotif
          actionMessage={this.state.action_message}
          actionStatus={this.state.action_status}
        />
        <div className="row row--mr-0">
          <div className="col">
            <div className="card">
              <div className="card-header">
                List Harga
                <Link to={'/price-create'}><button className="btn btn-success btn-sm" style={{float : 'right'}}>Tambah Harga</button></Link>
              </div>
              <div className="card-body">
                <table className="table table-striped table-price-list table-bordered table-price-list--bg-green">
                  <thead>
                    <tr>
                      <th rowSpan="2">Tanggal</th>
                      <th scope="col">Komoditas</th>
                      <th scope="col">Provinsi</th>
                      <th scope="col">Kota</th>
                      <th scope="col">Size</th>
                      <th scope="col">Price</th>
                      <th rowSpan="2" colSpan="2">Action</th>
                    </tr>
                    <tr>
                      {this.loopSearchBar()}
                    </tr>
                  </thead>
                  <tbody>
                      {this.state.price_list_pagination.map(list => 
                        <tr>
                          <th>{convertDateFormattoString(list.tgl_parsed)}</th>
                          <th scope="row">{list.komoditas}</th>
                          <td>{list.area_provinsi}</td>
                          <td>{list.area_kota}</td>
                          <td>{list.size}</td>
                          <td>{changeToFormatNumber(list.price)}</td>
                          <td className={'td-price-list--action'}>
                            <Link to={'/price-edit/'+list.uuid}><button className="btn btn-info btn-sm" id="edit-harga">Edit</button></Link>
                          </td>
                          <td className={'td-price-list--action'}>
                            <button type="button" id="delete-harga" onClick={() => this.toggleNotifyDeleteData(list.uuid)} className="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal">Hapus</button>
                          </td>
                        </tr>
                      )}
                  </tbody>
                </table>
                <div className="table-price-list-pagination">
                  <div className="row">
                  <div className="col-12 table-price-list__note-date--center">
                    <small>Menampilkan {this.state.price_list_pagination.length} data dari total {this.state.totalData} data</small>
                  </div>
                  <div className="col-12 table-price-list__pagination--center">
                    <Pagination
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.perPage}
                      totalItemsCount={this.state.totalData}
                      pageRangeDisplayed={3}
                      onChange={this.handlePageChange}
                      itemClass="page-item"
                      linkClass="page-link"
                    />
                  </div>
                  </div>
                </div>
                <div>
                {this.state.price_list_pagination.length !== 0 && (
                  <div className="mobile-card-price-list-pagination">
                    <div className="row">
                    <div className="col-12 mobile-card-price-list__note-date--center">
                      <small>Menampilkan {this.state.price_list_pagination.length} data dari total {this.state.totalData} data</small>
                    </div>
                    <div className="col-12 mobile-card-price-list__pagination--center">
                      <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.perPage}
                        totalItemsCount={this.state.totalData}
                        pageRangeDisplayed={3}
                        onChange={this.handlePageChange}
                        itemClass="page-item"
                        linkClass="page-link"
                      />
                    </div>
                    </div>
                  </div>
                )}
                <div class="mobile-card-price-list">
                {this.state.price_list_pagination.map(list => 
                  <div class="card card-price-list">
                    <div class="card-body">
                      <div className="row">
                        <div className="col-6 card-price-list-col-info--left">
                          <div className="row">
                            <div className="col card-price-list__komoditas">
                              <span>{list.komoditas}</span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col card-price-list__size">
                              <span>{"Size "+list.size+""}</span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col card-price-list__price">
                                <span>{changeToFormatNumber(list.price)}</span>
                              </div>
                          </div>
                        </div>
                        <div className="col-6 card-price-list-col-info--right">
                          <div className="row">
                            <div className="col card-price-list__provinsi">
                              <span>{list.area_provinsi}</span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col card-price-list__kota">
                                <span>{list.area_kota}</span>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-footer card-price-list-footer">
                      <div className="card-price-list-footer--flex-between">
                        <button type="button" id="delete-harga" onClick={() => this.toggleNotifyDeleteData(list.uuid)} className="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#deleteModal">Hapus</button>
                        <Link to={'/price-edit/'+list.uuid}><button id="edit-harga" className="btn btn-info btn-sm">Edit</button></Link>
                      </div>
                    </div>
                  </div>
                )}
                </div>
                <div className="mobile-card-price-list-pagination">
                  <div className="row">
                  <div className="col-12 mobile-card-price-list__note-date--center">
                    <small>Menampilkan {this.state.price_list_pagination.length} data dari total {this.state.totalData} data</small>
                  </div>
                  <div className="col-12 mobile-card-price-list__pagination--center">
                    <Pagination
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.perPage}
                      totalItemsCount={this.state.totalData}
                      pageRangeDisplayed={3}
                      onChange={this.handlePageChange}
                      itemClass="page-item"
                      linkClass="page-link"
                    />
                  </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div className={this.state.modal_notify_delete === true ? "modal fade show" : "modal fade"} id="deleteModal" tabIndex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true" style={this.state.modal_notify_delete === true ? {display : 'block'} : {display : 'none'} }>
          <div className="modal-dialog">
            <div className="modal-content modal-content--v-10p">
              <div className="modal-header modal-header--bg-danger">
                <h5 className="modal-title" id="deleteModalLabel">Konfirmasi</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={() => this.toggleNotifyDeleteData()}></button>
              </div>
              <div className="modal-body">
                Apakah anda yakin ingin menghapus data ini?
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary btn-sm" data-bs-dismiss="modal" onClick={() => this.toggleNotifyDeleteData()} style={{marginRight : '20px'}}>Close</button>
                <button type="button" className="btn btn-danger btn-sm" onClick={this.deleteDataHarga}>Hapus</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

  export default ListPrice;
  